const controller = require("../controllers/ctf.controller");
const { authJwt, admin } = require("../middleware");
const { body, query } = require('express-validator')

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/api/ctf/admin/signin", [
        body('npm').isNumeric(),
        body('password').whitelist(['abcdef','0123456789'])
    ], controller.signin);

    app.get("/api/ctf/getpoint", [authJwt.verifyToken],controller.getpoint);

    app.get("/api/ctf/leaderboard", [authJwt.verifyToken],controller.leaderboard);

    app.get("/api/ctf/allcategories", [authJwt.verifyToken],controller.allcategories);

    app.get("/api/ctf/alllevels", [authJwt.verifyToken],controller.alllevels);

    app.get("/api/ctf/allchallenges", [authJwt.verifyToken],controller.allchallenges);

    app.get("/api/ctf/getchallenge", [
        authJwt.verifyToken,
        query('challengeId').isNumeric()
    ],controller.getchallenge);

    app.post("/api/ctf/checkflag", [
        authJwt.verifyToken,
        body('challengeId').isNumeric(),
        body('flag').isLength({max: 255})
    ],controller.checkflag);

    app.post("/api/ctf/submit", [
        authJwt.verifyToken,
        body('challengeId').isNumeric(),
        // body('flag').isLength({max: 255}),
        body('writeup').isLength({max: 255})
    ],controller.submit);

    app.get("/api/ctf/allcompletions", [authJwt.verifyToken],controller.allcompletions);

    app.get("/api/ctf/completionsofchallenge", [
        authJwt.verifyToken,
        query('challengeId').isNumeric()
    ],controller.completionsofchallenge);

    app.get("/api/ctf/completionsofuser", [
        authJwt.verifyToken,
        query('npm').isNumeric()
    ],controller.completionsofuser);

    app.get("/api/ctf/mycompletions", [authJwt.verifyToken],controller.mycompletions);


    /*ADMIN ROUTES*/

    app.get("/api/ctf/getcategory", [
        authJwt.verifyToken,
        query('categoryId').isNumeric()
    ], controller.getcategory);

    app.post("/api/ctf/addcategory", [
        authJwt.verifyToken,
        admin.isCTFAdmin,
        body('name').isLength({min: 1, max: 255})
    ], controller.addcategory);

    app.post("/api/ctf/editcategory", [
        authJwt.verifyToken,
        admin.isCTFAdmin,
        body('categoryId').isNumeric(),
        body('name').isLength({min: 1, max: 255})
    ], controller.editcategory);

    app.post("/api/ctf/deletecategory", [
        authJwt.verifyToken,
        admin.isCTFAdmin,
        body('categoryId').isNumeric()
    ], controller.deletecategory);

    app.post("/api/ctf/newchallenge", [
        authJwt.verifyToken,
        admin.isCTFAdmin,
        body('title').isLength({min: 1, max: 255}),
        body('description').isLength({min: 1, max: 1000}),
        body('point').isNumeric(),
        body('categoryId').isNumeric(),
        body('levelId').isNumeric(),
        // body('flag').isLength({min: 1, max: 255})
    ], controller.newchallenge);

    app.post("/api/ctf/editchallenge", [
        authJwt.verifyToken,
        admin.isCTFAdmin,
        body('challengeId').isNumeric(),
        body('title').isLength({min: 1, max: 255}),
        body('description').isLength({min: 1, max: 1000}),
        body('point').isNumeric(),
        body('categoryId').isNumeric(),
        body('levelId').isNumeric(),
        // body('flag').isLength({max: 255})
    ], controller.editchallenge);

    app.post("/api/ctf/deletechallenge", [
        authJwt.verifyToken,
        admin.isCTFAdmin,
        body('challengeId').isNumeric()
    ], controller.deletechallenge);

    app.get("/api/ctf/completionsdetail", [
        authJwt.verifyToken,
        admin.isCTFAdmin,
        query('completionId').isNumeric()
    ],controller.completionsdetail);
};
