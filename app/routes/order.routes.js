const controller = require("../controllers/order.controller");
const { authJwt, config, filter } = require("../middleware");
const { body, query } = require('express-validator')

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/order/all", [authJwt.verifyToken], controller.all);

    app.get("/api/order/get", [
        authJwt.verifyToken,
        query('id').isNumeric()
    ], controller.get);

    app.post("/api/order/cancel", [
        authJwt.verifyToken,
        body('id').isNumeric()
    ], controller.cancel);

    app.post("/api/order/create", [
        authJwt.verifyToken,
        filter.dormCheck,
        config.commerceIsOpen,
        body('orders').whitelist(['itemIdquany', '0123456789', '\'\",[]{}'])
    ], controller.order);
};
