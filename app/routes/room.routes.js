const controller = require("../controllers/room.controller");
const { authJwt, config, admin } = require("../middleware");
const { query, body } = require('express-validator')

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/locuse/all", [authJwt.verifyToken], controller.getlocuses);

    app.get("/api/building/list", [
        authJwt.verifyToken,
        query('locuseId').isNumeric()
    ], controller.getbuildings);

    app.get("/api/room/list", [
        authJwt.verifyToken,
        query('buildingId').isNumeric()
    ], controller.getrooms);

    app.get("/api/room/getparameters", [
        authJwt.verifyToken,
    ], controller.getparameters);

    app.get("/api/room/getdata", [
        authJwt.verifyToken,
    ], controller.getdata);

    app.post("/api/room/form", [
        authJwt.verifyToken,
        config.roomcheckingIsOpen,
        body('roomId').isNumeric(),
        body('password').isLength({min: 1, max: 29}),
        body('violations').whitelist(['ids','0123456789','\'\",[]{}'])
    ], controller.newform);

    app.get("/api/room/report", [
        authJwt.verifyToken,
        admin.isRoomAdmin
    ], controller.getreport);

    app.get("/api/room/download", [
        authJwt.verifyToken,
        admin.isRoomAdmin
    ],controller.download);

    app.post("/api/room/formclear", [
        authJwt.verifyToken,
        admin.isRoomAdmin
    ],controller.formclear);
};
