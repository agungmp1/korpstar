const controller = require("../controllers/coin.controller");
const { authJwt, admin } = require("../middleware");
const { body, query } = require('express-validator')
const multer  = require('multer')
var upload = multer({ dest: `${__dirname}/../export/jdih/` })

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/coin/balance", [authJwt.verifyToken], controller.getbalance);

    app.get("/api/coin/moneyspent", [authJwt.verifyToken], controller.getmoneyspent);

    app.post("/api/coin/transfer", [
        authJwt.verifyToken,
        body('targetNpm').isNumeric(),
        body('value').isNumeric()
    ], controller.transfer);

    app.get("/api/coin/mytransfers", [authJwt.verifyToken], controller.mytransfers);

    // app.post("/api/coin/topup", upload.single("image"), [
    //     authJwt.verifyToken,
    //     body('value').isNumeric()
    // ], controller.topup);

    app.post("/api/coin/midtrans/createtoken", [
        authJwt.verifyToken,
        body('payment_type').isLength({min: 1, max: 14}), 
        body('value').isNumeric()
    ], controller.midtranscreatetoken);

    app.post("/api/coin/midtrans/recordtransaction", controller.midtransrecordtransaction);
    //
    // app.get("/api/coin/alltopup", [
    //     authJwt.verifyToken,
    //     admin.isCoinAdmin
    // ], controller.alltopup);
    //
    // app.post("/api/coin/accept", [
    //     authJwt.verifyToken,
    //     admin.isCoinAdmin,
    //     body('id').isNumeric()
    // ], controller.accept);
    //
    // app.post("/api/coin/deny", [
    //     authJwt.verifyToken,
    //     admin.isCoinAdmin
    // ], controller.deny);

    app.get("/api/coin/history", [authJwt.verifyToken], controller.history);

    app.get("/api/coin/allhistory", [
        authJwt.verifyToken,
        admin.isCoinAdmin
    ], controller.allhistory);

    app.get("/api/coin/transactiondetail", [
        authJwt.verifyToken,
        query('id').isNumeric()
    ], controller.transactiondetail);

    app.get("/api/coin/download/:token", controller.download);
};
