const controller = require("../controllers/payment.controller");
const { authJwt, admin, filter } = require("../middleware");
const { body } = require('express-validator')

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/payment/all", [
        authJwt.verifyToken,
        filter.dormCheck,
        admin.isCommerceAdmin
    ], controller.allpayment);

    app.post("/api/payment/accept", [
        authJwt.verifyToken,
        admin.isCommerceAdmin,
        body('id').isNumeric()
    ], controller.accept);

    app.post("/api/payment/deny", [
        authJwt.verifyToken,
        admin.isCommerceAdmin,
        body('id').isNumeric()
    ], controller.deny);

    app.post("/api/payment/deliver", [
        authJwt.verifyToken,
        admin.isCommerceAdmin,
        body('id').isNumeric()
    ], controller.setdelivered);

    app.post("/api/payment/reset", [
        authJwt.verifyToken,
        filter.dormCheck,
        admin.isCommerceAdmin
    ], controller.reset);

    app.get("/api/payment/download", [
        authJwt.verifyToken,
        filter.dormCheck,
        admin.isCommerceAdmin
    ], controller.download);
};
