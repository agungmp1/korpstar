const controller = require("../controllers/aspiration.controller");
const { authJwt, admin } = require("../middleware");
const { body, query, checkSchema } = require('express-validator')

var targetSchema = {
    "target": {
      in: 'body',
      matches: {
        options: [/\b(?:Penyelenggara|Biro Perencanaan Ketarunaan Humas dan Protokol|Biro Umum|Biro Rohani|Biro Akademik|Biro Olahraga|Biro Kesenian|Biro Perniagaan|Biro Kesejahteraan Taruna|Biro Sosial dan Lingkungan|Biro Asrama Putra|Biro Asrama Putri|Biro Pengembangan Teknologi|Biro Siber|Seksi Hukum dan Tata Usaha|Seksi Keamanan dan Ketertiban|Seksi Pembinaan Mental dan Kepribadian|Seksi Protokol dan Tradisi|Biro Administrasi dan Keuangan|Biro Operasional dan Media Informasi|Komisi I Aspirasi|Komisi II Pengawasan Kinerja|Komisi III Pengawasan Program Kerja|Komisi IV Audit)\b/],
        errorMessage: "Invalid target"
      }
    }
  }

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/aspiration/recap", [authJwt.verifyToken], controller.recap);

    app.get("/api/aspiration/all", [authJwt.verifyToken], controller.all);

    app.post("/api/aspiration/create", [
        authJwt.verifyToken,
        body('title').isLength({ min: 1, max:254 }),
        body('argument').isLength({ min: 1, max:500 }),
        body('suggestion').isLength({ min: 1, max:500 }),
        body('proof').isLength({ min: 1, max:500 }),
        checkSchema(targetSchema)
    ], controller.create);

    app.post("/api/aspiration/destroy", [
        authJwt.verifyToken,
        // admin.isAspirationAdmin,
        body('aspirationId').isNumeric()
    ], controller.destroy);

    app.post("/api/aspiration/clearinactive", [
        authJwt.verifyToken,
        admin.isAspirationAdmin
    ], controller.clearinactive);

    app.get("/api/aspiration/download", [
        authJwt.verifyToken,
        admin.isAspirationAdmin
    ], controller.downloadreport);

    app.post("/api/aspiration/upvote", [
        authJwt.verifyToken,
        body('aspirationId').isNumeric()
    ], controller.upvote);

    app.post("/api/aspiration/downvote", [
        authJwt.verifyToken,
        body('aspirationId').isNumeric()
    ], controller.downvote);

    app.post("/api/aspiration/unvote", [
        authJwt.verifyToken,
        body('aspirationId').isNumeric()
    ], controller.unvote);

    app.get("/api/aspiration/request", [
        authJwt.verifyToken,
        admin.isAspirationAdmin
    ], controller.request);

    app.post("/api/aspiration/accept", [
        authJwt.verifyToken,
        body('aspirationId').isNumeric(),
        admin.isAspirationAdmin
    ], controller.accept);

    app.post("/api/aspiration/deny", [
        authJwt.verifyToken,
        body('aspirationId').isNumeric(),
        admin.isAspirationAdmin
    ], controller.deny);

    app.post("/api/aspiration/edit", [
        authJwt.verifyToken,
        body('aspirationId').isNumeric(),
        body('title').isLength({ min: 1, max:254 }),
        body('argument').isLength({ min: 1, max:500 }),
        body('suggestion').isLength({ min: 1, max:500 }),
        body('proof').isLength({ min: 1, max:500 }),
        checkSchema(targetSchema),
        admin.isAspirationAdmin
    ], controller.edit);

    app.post("/api/aspiration/activate", [
        authJwt.verifyToken,
        body('aspirationId').isNumeric(),
        admin.isAspirationAdmin
    ], controller.activate);

    app.post("/api/aspiration/deactivate", [
        authJwt.verifyToken,
        body('aspirationId').isNumeric(),
        admin.isAspirationAdmin
    ], controller.deactivate);

    app.get("/api/aspiration/voterecap", [
        authJwt.verifyToken,
        query('aspirationId').isNumeric(),
    ], controller.voterecap);

    app.get("/api/aspiration/unvoted", [
        authJwt.verifyToken,
        admin.isAspirationAdmin
    ], controller.unvoted);
};
