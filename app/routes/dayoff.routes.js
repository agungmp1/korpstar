const controller = require("../controllers/dayoff.controller");
const { authJwt, admin, config } = require("../middleware");
const { body, query, checkSchema } = require('express-validator')

var registerSchema = {
    "type": {
        in: 'body',
        matches: {
            options: [/\b(?:Pesiar|Bermalam)\b/],
            errorMessage: "Invalid type"
        }
    },
    "transportation": {
        in: 'body',
        matches: {
            options: [/\b(?:Embarkasi|Mandiri)\b/],
            errorMessage: "Invalid transportation"
        }
    },
    // "pesiartype": {
    //     in: 'body',
    //     optional: { options: { nullable: true } },
    //     matches: {
    //         options: [/\b(?:Pesiar pagi|Pesiar siang)\b/],
    //         errorMessage: "Invalid pesiartype"
    //     }
    // }
}

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/api/dayoff/register", [
        authJwt.verifyToken,
        body('date').isLength({ min: 1 }),
        body('destinationaddress').isLength({ min: 1 }),
        body('selfcontact').isLength({ min: 1 }),
        checkSchema(registerSchema),
        config.dayoffIsOpen
    ], controller.register);

    app.post("/api/dayoff/cancel", [
        authJwt.verifyToken,
        body('id').isNumeric(),
        config.dayoffIsOpen
    ], controller.cancel);

    // app.post("/api/dayoff/accept", [
    //     authJwt.verifyToken,
    //     body('id').isNumeric(),
    //     admin.isDayOffAdmin
    // ], controller.accept);

    // app.post("/api/dayoff/deny", [
    //     authJwt.verifyToken,
    //     body('id').isNumeric(),
    //     admin.isDayOffAdmin
    // ], controller.deny);

    // app.post("/api/dayoff/setarrived", [
    //     authJwt.verifyToken,
    //     body('id').isNumeric(),
    //     admin.isDayOffAdmin
    // ], controller.setarrived);

    app.get("/api/dayoff/list", [
        authJwt.verifyToken,
    ], controller.list);

    app.post("/api/dayoff/clear", [
        authJwt.verifyToken,
        admin.isDayOffAdmin
    ], controller.clear);

    app.get("/api/dayoff/download", [
        authJwt.verifyToken,
        admin.isDayOffAdmin
    ], controller.download);
};
