const controller = require("../controllers/foul.controller");
const { authJwt, admin } = require("../middleware");
const { body, query } = require('express-validator')

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/foul/recap", [
        authJwt.verifyToken,
        query('monthyear').whitelist(['-','0123456789'])
    ], controller.recap);

    app.get("/api/foul/linegraph", [
        authJwt.verifyToken
    ], controller.linegraph);

    app.get("/api/foul/all", [
        authJwt.verifyToken,
        admin.hasFoulPassword
    ], controller.all);

    app.get("/api/foul/list", [
        authJwt.verifyToken
    ], controller.getmyfouls);

    app.post("/api/foul/add", [
        authJwt.verifyToken,
        admin.hasFoulPassword,
        body('npm').isNumeric(),
        body('foulId').isNumeric(),
        body('date').isLength({min: 1, max: 29})
    ], controller.add);

    app.get("/api/foul/dead", [
        authJwt.verifyToken
    ], controller.getdeadusers);

    app.post("/api/foul/revive", [
        authJwt.verifyToken,
        admin.isFoulAdmin,
        body('npm').isNumeric(),
    ], controller.revive);

    app.post("/api/foul/destroy", [
        authJwt.verifyToken,
        admin.hasFoulPassword,
        body('id').isNumeric()
    ], controller.destroy);

    app.get("/api/foul/download", [
        authJwt.verifyToken,
        query('monthyear').whitelist(['-','0123456789'])
    ], controller.download);
};
