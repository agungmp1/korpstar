const controller = require("../controllers/item.controller");
const { authJwt, filter, admin } = require("../middleware");
const { body, query } = require('express-validator')

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/item/all", [
        authJwt.verifyToken,
        filter.dormCheck
    ], controller.all);

    app.get("/api/item/get", [
        authJwt.verifyToken,
        query('id').isNumeric()
    ], controller.get);

    app.post("/api/item/create", [
        authJwt.verifyToken,
        admin.isCommerceAdmin,
        filter.dormCheck,
        body('name').isLength({ min: 1, max: 100 }),
        body('stock').isNumeric(),
        body('iconId').isNumeric(),
        body('price').isNumeric()
    ], controller.create);

    app.post("/api/item/edit", [
        authJwt.verifyToken,
        admin.isCommerceAdmin,
        body('id').isNumeric(),
        body('name').isLength({ min: 1, max: 100 }),
        body('stock').isNumeric(),
        body('iconId').isNumeric(),
        body('price').isNumeric()
    ], controller.edit);

    app.post("/api/item/destroy", [
        authJwt.verifyToken,
        admin.isCommerceAdmin,
        body('id').isNumeric()
    ], controller.destroy);

    app.post("/api/item/clear", [
        authJwt.verifyToken,
        filter.dormCheck,
        admin.isCommerceAdmin
    ], controller.clear);
};
