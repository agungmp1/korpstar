const db = require("../models");
const User = db.user;
const Room = db.class;
const Building = db.classmeeting;
const Book = db.book;
const BookLending = db.booklending;
const Role = db.role;
const Sequelize = db.Sequelize;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

const moment = require('moment')

exports.get = (req, res) => {
    User.findOne({
        where: { npm: req.npm },
        include: [{
            model: Role,
            attributes: ['id']
        }, {
            model: BookLending,
            include: Book
        }]
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 15) {
                isAdmin = true;
            }
        });
        lendings = []
        user.book_lendings.forEach(lending => {
            lendings.push({
                id: lending.id,
                book: lending.book,
                start: lending.createdAt,
                status: (lending.status != 'DONE' && lending.createdAt < moment().subtract(1, 'w')) ? 'OVERDUE' : lending.status
            })
        });
        res.send({ lendings: lendings, isAdmin: isAdmin, building: req.building.name })
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.setbooked = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Book.findOne({
        where: { id: req.body.bookId },
        include: [BookLending]
    }).then(book => {
        let available = true
        book.book_lendings.forEach(lending => {
            if (lending.status != 'DONE') available = false;
        });
        if (!available || book.buildingId != req.buildingId) {
            return res.status(422).send({ message: "Book currently unavailable" })
        }
        BookLending.create({
            bookId: req.body.bookId,
            userNpm: req.npm,
            status: 'BOOKED',
        }).then(x => {
            console.log(`[book booking][${new Date()}] ${req.npm} booked a book #${req.body.id}`);
            res.send({ message: "Booked succesfully!" })
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });

}

exports.cancel = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    BookLending.findOne({ where: { id: req.body.id } }).then(record => {
        if (record.status !== "BOOKED") {
            return res.status(422).send({ message: "Rule violation!" });
        }
        BookLending.destroy({ where: { id: req.body.id } }).then(x => {
            console.log(`[book cancel][${new Date()}] ${req.npm} cancel booking a book #${req.body.id}`);
            res.status(200).send({ message: "Cancelled successfully!" });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
}

exports.setongoing = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    BookLending.findOne({ where: { id: req.body.id } }).then(record => {
        if (record.status !== "BOOKED") {
            return res.status(422).send({ message: "Rule violation!" });
        }
        BookLending.update({
            status: 'ON GOING',
        }, { where: { id: req.body.id } }).then(x => {
            console.log(`[book borrowing on going][${new Date()}] ${req.npm} start borrowing a book #${req.body.id}`);
            res.status(200).send({ message: "Updated successfully!" });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
}

exports.setdone = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    BookLending.findOne({ where: { id: req.body.id } }).then(record => {
        if (record.status !== "ON GOING") {
            return res.status(422).send({ message: "Rule violation!" });
        }
        BookLending.update({
            status: 'DONE',
        }, { where: { id: req.body.id } }).then(x => {
            console.log(`[book borrowing done][${new Date()}] ${req.npm} return a book #${req.body.id}`);
            res.status(200).send({ message: "Updated successfully!" });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
}