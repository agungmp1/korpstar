const db = require("../models");
const JDIH = db.jdih;
const User = db.user;
const Role = db.role;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');
const fs = require('fs-extra');

exports.all = (req, res) => {
    JDIH.findAll({raw: true}).then( jdihs => {
        User.findOne({
            where: {npm: req.npm},
            include: {
                model: Role,
                attributes: ['id']
            }
        }).then(user => {
            let isAdmin = false;
            user.roles.forEach((role) => {
                if (role.id == 6) {
                    isAdmin = true;
                }
            });
            res.status(200).send({data: jdihs, isAdmin: isAdmin});
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.download = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    JDIH.findOne({
        where: {id: req.query.id}
    }).then( jdih => {
        const file = `${__dirname}/../export/jdih/${jdih.file}`;
        res.download(file);
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.new = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    var src = fs.createReadStream(req.file.path);
    var dest = fs.createWriteStream(`${__dirname}/../export/jdih/${req.file.originalname}`);
    src.pipe(dest);
    src.on('end', function() {
    	fs.unlinkSync(req.file.path);
        JDIH.create({
            name: req.body.name,
            file: req.file.originalname
        }).then(jdih => {
            console.log(`[jdih input][${new Date()}] ${req.npm} input jdih#${jdih.id} :${req.body.name}`);
            res.status(200).send({ message: "Added successfully!" });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    });
    src.on('error', function(err) { res.status(500).send({err: err}); });
};

exports.destroy = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    JDIH.findOne({
        where: {id: req.body.id}
    }).then( jdih => {
        const file = `${__dirname}/../export/jdih/${jdih.file}`;
        fs.remove(file).then(() => {
            JDIH.destroy({ where: {id: req.body.id}}).then( data => {
                console.log(`[jdih delete][${new Date()}] ${req.npm} delete jdih#${req.body.id}`);
                res.status(200).send({ message: "Destroyed successfully!" });
            }).catch(err => {
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.clear = (req, res) => {
    JDIH.destroy({ where: {}}).then( data => {
        console.log(`[jdih delete][${new Date()}] ${req.npm} clear jdih data`);
        res.status(200).send({ message: "Cleared successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}
