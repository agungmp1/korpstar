const db = require("../models");
const CTFCategory = db.ctfcategory;
const CTFLevel = db.ctflevel;
const CTFChallenge = db.ctfchallenge;
const CTFChallengeCompletion = db.ctfchallengecompletion;
const User = db.user;
const Class = db.class;
const Role = db.role;
const Sequelize = db.Sequelize;
const sequelize = db.sequelize;

var jwt = require("jsonwebtoken");
const config = require("../config/auth.config");
var bcrypt = require("bcryptjs");

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

exports.signin = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({
            where: {
                npm: req.body.npm
            },
            include: {
                model: Role,
                attributes: ['id', 'name']
            }
        })
        .then(user => {
            if (!user) {
                return res.status(404).send({ message: "User Not found." });
            }
            var passwordIsValid = bcrypt.compareSync(
                req.body.password + req.body.npm,
                user.password
            );

            if (!passwordIsValid) {
                return res.status(401).send({
                    accessToken: null,
                    message: "Invalid Password!"
                });
            }
            let isAdmin = false;
            user.roles.forEach((role) => {
                if (role.id == 7) {
                    isAdmin = true;
                }
            });
            if (!isAdmin) {
                return res.status(403).send({
                    accessToken: null,
                    message: "Insufficient privilege!"
                });
            }
            var token = jwt.sign({ npm: user.npm }, config.secret, {
                expiresIn: 86400 // 24 hours
            });

            res.status(200).send({
                npm: user.npm,
                accessToken: token
            });
        })
        .catch(err => {
            res.status(500).send({ message: err.message });
        });
};

exports.getpoint = (req, res) => {
    User.findOne({
        where:{npm: req.npm},
        attributes: ['ctf_point']
    }).then( user => {
        res.status(200).send({point: user.ctf_point});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.leaderboard = (req, res) => {
    User.findAll({
        attributes: ['npm','fullname','ctf_point'],
        include: { model: Class },
        order: [['ctf_point','DESC'], ['npm', 'ASC']]
    }).then( users => {
        let data = []
        users.forEach((user) => {
            data.push({
                npm: user.npm,
                fullname: user.fullname,
                ctf_point: user.ctf_point,
                class: user.class.name
            })
        });
        sequelize.query(`SELECT * FROM (SELECT npm, ctf_point, rank from (SELECT npm, ctf_point, @tmprank := @tmprank + 1 AS rank FROM users CROSS JOIN (SELECT @tmprank := 0) init ORDER BY ctf_point DESC ) rt ORDER BY rank ) AS r WHERE r.npm = ${req.npm};`, { type: Sequelize.QueryTypes.SELECT }).then( x => {
            res.status(200).send({data: data, rank: x.rank});
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.allcategories = (req, res) => {
    CTFCategory.findAll({order: [['id','ASC']]}).then( categories => {
        res.status(200).send({data: categories});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.alllevels = (req, res) => {
    CTFLevel.findAll({order: [['id','ASC']]}).then( levels => {
        res.status(200).send({data: levels});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.allchallenges = (req, res) => {
    let where = {};
    if (req.body.categoryId) {
        if(!isNaN(req.body.categoryId)){
            where = {ctfCategoryId: req.body.categoryId};
        }
    }
    // console.log(where);
    CTFChallenge.findAll({
        where: where,
        attributes: [
            'id', 'title', 'point', 'ctfCategoryId', 'ctfLevelId',
            // [Sequelize.fn("COUNT", Sequelize.col("ctf_challenge_completions.id")), "solvesCount"],
            'createdAt'
        ],
        include: [{
            model: CTFCategory,
            attributes: ['id', 'name']
        },{
            model: CTFLevel,
            attributes: ['id', 'name']
        },{
            model: CTFChallengeCompletion,
            attributes: ['id']
        }]
    }).then( challenges => {
        data = []
        challenges.forEach((challenge) => {
            solvesCount = 0;
            challenge.ctf_challenge_completions.forEach((completion) => {
                solvesCount++;
            });
            data.push({
                id: challenge.id,
                title: challenge.title,
                point: challenge.point,
                level_id: challenge.ctfLevelId,
                level_name: challenge.ctf_level.name,
                category_id: challenge.ctfCategoryId,
                category_name: challenge.ctf_category.name,
                createdAt: challenge.createdAt,
                solvesCount: solvesCount
            })
        });

        // console.log(challenges);
        res.status(200).send({data: data});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.getchallenge = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFChallenge.findOne({
        where:{id: req.query.challengeId},
        attributes: [
            'id', 'title', 'description','point',
            [Sequelize.fn("COUNT", Sequelize.col("ctf_challenge_completions.id")), "solvesCount"],
            'createdAt'
        ],
        include: [{
            model: CTFCategory,
            attributes: ['id', 'name']
        },{
            model: CTFLevel,
            attributes: ['id', 'name']
        },{
            model: CTFChallengeCompletion,
            attributes: ['id']
        }]
    }).then( challenge => {
        CTFChallengeCompletion.findOne({where:{userNpm:req.npm, ctfChallengeId: req.query.challengeId}}).then( completion => {
            let isSolved = false;
            if (completion) {
                isSolved = true;
            }
            res.status(200).send({data: challenge, isSolved: isSolved});
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });;
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.checkflag = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFChallenge.findOne({where:{id:req.body.challengeId}, attributes:['flag']}).then( challenge => {
        var isTrue = bcrypt.compareSync( req.body.flag, challenge.flag );
        res.status(200).send({isTrue: isTrue});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.submit = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFChallenge.findOne({where:{id:req.body.challengeId}, attributes:['id','point','flag']}).then( challenge => {
        if (bcrypt.compareSync( req.body.flag, challenge.flag )) {
            CTFChallengeCompletion.findOne({where:{userNpm:req.npm, ctfChallengeId: challenge.id}}).then( completion => {
                if (!completion) {
                    CTFChallengeCompletion.create({
                        ctfChallengeId: challenge.id,
                        userNpm: req.npm,
                        writeup: req.body.writeup
                    }).then(record => {
                        User.update({
                            ctf_point: Sequelize.literal(`ctf_point + ${challenge.point}`)
                        },{where: {npm:req.npm}}).then(user => {
                            res.status(200).send({ message: "Point saved" });
                        }).catch(err => {
                            res.status(500).send({ message: err.message });
                        });
                    }).catch(err => {
                        res.status(500).send({ message: err.message });
                    });
                } else {
                    res.status(422).send({ message: "Challenge already solved"});
                }
            }).catch(err => {
                res.status(500).send({ message: err.message });
            });
        } else {
            res.status(422).send({ message: "dont play play bossqueh" });
        }
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.allcompletions = (req, res) => {
    let where = {};
    if (req.query.date_from && req.query.date_to) {
        where = {
            createdAt: {
                [Op.gte]: req.query.date_from,
                [Op.lte]: req.query.date_to
            }
        }
    } else if (req.query.date_from) {
        where = {
            createdAt: {
                [Op.gte]: req.query.date_from,
            }
        }
    } else if (req.query.date_to) {
        where = {
            createdAt: {
                [Op.lte]: req.query.date_to,
            }
        }
    }
    CTFChallengeCompletion.findAll({
        attributes:['id', 'createdAt'],
        include: [{
            model: User,
            attributes: ['npm', 'fullname'],
            include: { model: Class }
        },{
            model: CTFChallenge,
            attributes: ['id', 'title','point'],
            include: [{
                model: CTFCategory,
                attributes: ['id', 'name']
            },{
                model: CTFLevel,
                attributes: ['id', 'name']
            }]
        }],
        where: where
    }).then( completions => {
        completions.forEach((completion) => {
            completion.user.class = completion.user.class.name
        });
        res.status(200).send({data: completions});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.completionsofchallenge = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFChallengeCompletion.findAll({
        where:{ctfChallengeId: req.query.challengeId},
        attributes:['id', 'createdAt'],
        include: {
            model: User,
            attributes: ['npm', 'fullname']
        }
    }).then( completions => {
        res.status(200).send({data: completions});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.completionsofuser = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFChallengeCompletion.findAll({
        where:{userNpm: req.query.npm},
        attributes:['id', 'createdAt'],
        include: {
            model: CTFChallenge,
            attributes: ['id', 'title'],
            include: [{
                model: CTFCategory,
                attributes: ['id', 'name']
            },{
                model: CTFLevel,
                attributes: ['id', 'name']
            }]
        }
    }).then( completions => {
        res.status(200).send({data: completions});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.mycompletions = (req, res) => {
    CTFChallengeCompletion.findAll({
        where:{userNpm: req.npm},
        attributes:['id', 'writeup', 'createdAt'],
        include: {
            model: CTFChallenge,
            attributes: ['id', 'title'],
            include: [{
                model: CTFCategory,
                attributes: ['id', 'name']
            },{
                model: CTFLevel,
                attributes: ['id', 'name']
            }]
        }
    }).then( completions => {
        res.status(200).send({data: completions});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

/*ADMIN FUNCTIONS*/

exports.getcategory = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFCategory.findOne({ where: {id: req.query.categoryId}}).then( category => {
        res.status(200).send({
            id: category.id,
            name: category.name
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.addcategory = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFCategory.create({
        name: req.body.name
    }).then( category => {
        res.status(200).send({ message: "Created successfully"});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.editcategory = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFCategory.update({
        name: req.body.name
    },{where: {id:req.body.categoryId}}).then( category => {
        res.status(200).send({ message: "Updated successfully"});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.deletecategory = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFCategory.destroy({where: {id:req.body.categoryId}}).then( category => {
        res.status(200).send({ message: "Deleted successfully"});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.newchallenge = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFChallenge.create({
        title: req.body.title,
        description: req.body.description,
        point: req.body.point,
        ctfCategoryId: req.body.categoryId,
        ctfLevelId: req.body.levelId,
        flag: bcrypt.hashSync(req.body.flag, 8)
    }).then( challenge => {
        res.status(200).send({ message: "Created successfully"});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.editchallenge = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFChallenge.update({
        title: req.body.title,
        description: req.body.description,
        point: req.body.point,
        ctfCategoryId: req.body.categoryId,
        ctfLevelId: req.body.levelId,
        flag: bcrypt.hashSync(req.body.flag, 8)
    },{where: {id:req.body.challengeId}}).then( challenge => {
        res.status(200).send({ message: "Updated successfully"});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.deletechallenge = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFChallenge.destroy({where: {id:req.body.challengeId}}).then( category => {
        res.status(200).send({ message: "Deleted successfully"});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.completionsdetail = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    CTFChallengeCompletion.findOne({
        where:{id: req.query.completionId},
        attributes:['id', 'writeup', 'createdAt'],
        include: [{
            model: User,
            attributes: ['npm', 'fullname']
        },{
            model: CTFChallenge,
            attributes: ['id', 'title'],
            include: [{
                model: CTFCategory,
                attributes: ['id', 'name']
            },{
                model: CTFLevel,
                attributes: ['id', 'name']
            }]
        }]
    }).then( completion => {
        res.status(200).send({data: completion});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};
