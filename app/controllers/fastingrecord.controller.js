const db = require("../models");
const User = db.user;
const Class = db.class;
const Role = db.role;
const Room = db.room;
const Building = db.building;
const Locus = db.locus;
const FastingRecord = db.fastingrecord;
const Config = db.config;

const Op = db.Sequelize.Op;
const csvwriter = require("csv-writer");
const createCsvWriter = csvwriter.createObjectCsvWriter;

exports.register = (req, res) => {
    FastingRecord.create({ userNpm: req.npm }).then(user => {
        res.status(200).send({ message: "Registered successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.cancel = (req, res) => {
    FastingRecord.destroy({ where: {userNpm: req.npm }}).then(user => {
        res.status(200).send({ message: "Cancelled successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.list = (req, res) => {
    FastingRecord.findAll({
        include: {
            model: User,
            attributes: ['fullname'],
            include: { model: Class }
        }
    }).then( fastingrecords => {
        Config.findOne({where: {id: 1} }).then(config => {
            User.findOne({
                where: {npm: req.npm},
                attributes: ['npm'],
                include: {
                    model: Role,
                    attributes: ['id', 'name']
                }
            }).then( user => {
                let isRegistered = false;
                let isAdmin = false;
                user.roles.forEach((role) => {
                    if (role.id == 1){
                        isAdmin = true;
                    }
                });
                let data = []
                fastingrecords.forEach((record) => {
                    if (record.userNpm == req.npm){
                        isRegistered = true;
                    }
                    data.push({
                        npm: record.npm,
                        user:{
                            fullname: record.user.fullname,
                            class: record.user.class.name
                        },
                    })
                });
                res.status(200).send({
                    data: data,
                    date: config.fastingdate,
                    isOpened: config.fastingopen,
                    isAdmin: isAdmin,
                    isRegistered: isRegistered
                });
            });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.total = (req, res) => {
    FastingRecord.findAndCountAll({
        include: {
            model: User
        }
    }).then( fastingrecords => {
        let isRegistered = false;
        fastingrecords.rows.forEach((record) => {
            if (record.userNpm == req.npm){
                isRegistered = true;
            }
        });
        res.status(200).send({
            total: fastingrecords.count,
            isRegistered: isRegistered
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.clear = (req, res) => {
    FastingRecord.destroy({ where: {}}).then( data => {
        console.log(`[fasting reset][${new Date()}] ${req.npm} reset fasting registration`);
        res.status(200).send({ message: "Cleared successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

const zip = require('express-zip');
exports.download = (req, res) => {
    Config.findOne({where: {id: 1} }).then(config => {
        FastingRecord.findAll({
            include: {
                model: User,
                attributes: ['fullname'],
                include: [{
                    model: Room,
                    attributes: ['number'],
                    include: {
                        model: Building,
                        attributes: ['name'],
                        include: {
                            model: Locus,
                            attributes: ['name'],
                        }
                    }
                }, {
                    model: Class
                }]
            }
        }).then(fastingrecords => {
            array = []
            fastingrecords.forEach((record) => {
                array.push({
                    npm: record.npm,
                    fullname: record.user.fullname,
                    class: record.user.class.name,
                    room: !record.user.room ? null : record.user.room.number,
                    building: !record.user.room ? null : record.user.room.building.name,
                    locus: !record.user.room ? null : record.user.room.building.locuse.name
                })
            });

            const csvWriter = createCsvWriter({
                path: `${__dirname}/../export/fastingrecord/datapuasa_${config.fastingdate}.csv`,
                header: [
                    { id: "npm", title: "npm" },
                    { id: "fullname", title: "fullname" },
                    { id: "class", title: "class" },
                    { id: "room", title: "room" },
                    { id: "building", title: "building" },
                    { id: "locus", title: "locus" },
                ]
            });
            csvWriter.writeRecords(array).then(() => {
                const file = `${__dirname}/../export/fastingrecord/datapuasa_${config.fastingdate}.csv`;
                console.log(`[fasting download][${new Date()}] ${req.npm} download fasting data`);
                res.zip([{
                    path: file,
                    name: `datapuasa_${config.fastingdate}.csv`
                }]);
            });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};
