const db = require("../models");
const { sendNotification } = require('../utils/fcm.js')
const User = db.user;
const FCMReToken = db.fcmretoken;
const Sequelize = db.Sequelize;

const Op = db.Sequelize.Op;
const {
    validationResult
} = require('express-validator');
const e = require("express");

exports.create = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({
            message: "Invalid input",
            errors: errors.array()
        })
    }
    FCMReToken.findOne({
        where: {
            token: req.body.token
        }
    }).then(token => {
        if (token) {
            FCMReToken.update({ userNpm: req.npm }, {
                where: {
                    token: req.body.token
                }
            }).then(x => {
                // sendNotification(req.body.token, {
                //     notification: {
                //         title: 'Welcome',
                //         body: 'Your device is registered for notif'
                //     }
                // })
                res.status(200).send({
                    message: "Token refreshed"
                });
            }).catch(err => {
                console.log(err);
                res.status(500).send({
                    message: err.message
                });
            });
        } else {
            FCMReToken.create({
                userNpm: req.npm,
                token: req.body.token
            }).then(x => {
                // sendNotification(req.body.token, {
                //     notification: {
                //         title: 'Welcome',
                //         body: 'Your device is registered for notif'
                //     }
                // })
                res.status(200).send({
                    message: "Token registered"
                });
            }).catch(err => {
                console.log(err);
                res.status(500).send({
                    message: err.message
                });
            });
        }
    }).catch(err => {
        console.log(err);
        res.status(500).send({
            message: err.message
        });
    });
};

const moment = require('moment')
exports.clearInactive = (req, res) => {
    FCMReToken.destroy({
        where: {
            updatedAt: {
                [Op.lte]: moment().subtract(14, 'days')
            }
        }
    }).then(token => {
        console.log(`[fcm token][${new Date()}] fcm token clear inactive`);
    }).catch(err => {
        console.log(err);
    });
};