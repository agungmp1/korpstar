const db = require("../models");
const User = db.user;
const rp = require('request-promise');
const jwt = require('jsonwebtoken');

const zoomConfig = require("../config/zoom.config.js");

const Op = db.Sequelize.Op;
const Sequelize = db.Sequelize;
const { validationResult } = require('express-validator');
const fs = require('fs-extra');

exports.newmeeting = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    const payload = {
        iss: zoomConfig.API_KEY,
        exp: ((new Date()).getTime() + 500000)
    };
    const token = jwt.sign(payload, zoomConfig.API_SECRET);
    const email = zoomConfig.email;
    var options = {
        method: "POST",
        uri: "https://api.zoom.us/v2/users/" + email + "/meetings",
        body: {
            topic: "Karya Zoom",
            type: 2,
            start_time: "2022-03-30T12:00:00",
            timezone: "Asia/Jakarta",
            duration: 60,
            password: "gasngezoom",
            agenda: "gitu deh",
            "settings": {
                "host_video": true,
                "participant_video": true,
                "cn_meeting": false,
                "in_meeting": true,
                "join_before_host": false,
                "mute_upon_entry": false,
                "watermark": false,
                "use_pmi": false,
                "approval_type": 2,
                "audio": "both",
                "auto_recording": "local",
                "enforce_login": false,
                "registrants_email_notification": false,
                "waiting_room": true,
                "allow_multiple_devices": true
            },
        },
        auth: {
            bearer: token
        },
        headers: {
            "User-Agent": "Zoom-api-Jwt-Request",
            "content-type": "application/json"
        },
        json: true //Parse the JSON string in the response
    };

    rp(options)
    .then(function(response) {
        res.send(response);
    })
    .catch(function(err) {
        console.log(err);
        res.status(500).send({message: err.message})
    });
};

exports.allmeetings = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    const payload = {
        iss: zoomConfig.API_KEY,
        exp: ((new Date()).getTime() + 500000)
    };
    const token = jwt.sign(payload, zoomConfig.API_SECRET);
    const email = zoomConfig.email;
    // console.log(token);
    var options = {
        method: "GET",
        uri: "https://api.zoom.us/v2/users/" + email + "/meetings?type=upcoming",
        auth: {
            bearer: token
        },
        query: {
            type: "upcoming"
        },
        headers: {
            "User-Agent": "Zoom-api-Jwt-Request",
            "content-type": "application/json"
        },
        json: true //Parse the JSON string in the response
    };

    rp(options)
    .then(function(response) {
        res.send(response);
    })
    .catch(function(err) {
        console.log(err);
        res.status(500).send({message: err.message})
    });
};

exports.updatemeeting = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    const payload = {
        iss: zoomConfig.API_KEY,
        exp: ((new Date()).getTime() + 500000)
    };
    const token = jwt.sign(payload, zoomConfig.API_SECRET);
    const email = zoomConfig.email;
    // console.log(token);
    var options = {
        method: "PATCH",
        uri: `https://api.zoom.us/v2/meetings/88303413457`,
        // uri: `https://api.zoom.us/v2/meetings/${req.body.meetingId}`,
        auth: {
            bearer: token
        },
        body: {
            topic: "Karya Zoom",
            type: 2,
            start_time: "2022-03-30T11:00:00",
            timezone: "Asia/Jakarta",
            duration: 60,
            password: "gasngezoom",
            agenda: "gitu deh",
            "settings": {
                "host_video": true,
                "participant_video": true,
                "cn_meeting": false,
                "in_meeting": true,
                "join_before_host": false,
                "mute_upon_entry": false,
                "watermark": false,
                "use_pmi": false,
                "approval_type": 2,
                "audio": "both",
                "auto_recording": "local",
                "enforce_login": false,
                "registrants_email_notification": false,
                "waiting_room": true,
                "allow_multiple_devices": true
            },
        },
        headers: {
            "User-Agent": "Zoom-api-Jwt-Request",
            "content-type": "application/json"
        },
        json: true //Parse the JSON string in the response
    };

    rp(options)
    .then(function(response) {
        res.send({message: "updated succesfully"});
    })
    .catch(function(err) {
        console.log(err);
        res.status(500).send({message: err.message})
    });
};

exports.deletemeeting = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    const payload = {
        iss: zoomConfig.API_KEY,
        exp: ((new Date()).getTime() + 500000)
    };
    const token = jwt.sign(payload, zoomConfig.API_SECRET);
    const email = zoomConfig.email;
    // console.log(token);
    var options = {
        method: "DELETE",
        uri: `https://api.zoom.us/v2/meetings/88303413457`,
        // uri: `https://api.zoom.us/v2/meetings/${req.body.meetingId}`,
        auth: {
            bearer: token
        },
        headers: {
            "User-Agent": "Zoom-api-Jwt-Request",
            "content-type": "application/json"
        },
        json: true //Parse the JSON string in the response
    };

    rp(options)
    .then(function(response) {
        res.send({message: "deleted succesfully"});
    })
    .catch(function(err) {
        console.log(err);
        res.status(500).send({message: err.message})
    });
};

exports.meetinginfo = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    const payload = {
        iss: zoomConfig.API_KEY,
        exp: ((new Date()).getTime() + 500000)
    };
    const token = jwt.sign(payload, zoomConfig.API_SECRET);
    const email = zoomConfig.email;
    // console.log(token);
    var options = {
        method: "GET",
        uri: `https://api.zoom.us/v2/meetings/88303413457`,
        // uri: `https://api.zoom.us/v2/meetings/${req.body.meetingId}`,
        auth: {
            bearer: token
        },
        headers: {
            "User-Agent": "Zoom-api-Jwt-Request",
            "content-type": "application/json"
        },
        json: true //Parse the JSON string in the response
    };

    rp(options)
    .then(function(response) {
        res.send(response);
    })
    .catch(function(err) {
        console.log(err);
        res.status(500).send({message: err.message})
    });
};
