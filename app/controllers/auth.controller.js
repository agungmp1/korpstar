const db = require("../models");
const config = require("../config/auth.config");
const Config = db.config;
const User = db.user;
const Role = db.role;

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const { validationResult } = require('express-validator');

exports.signin = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({
        where: {
            npm: req.body.npm
        }
    })
        .then(user => {
            if (!user) {
                return res.status(404).send({ message: "User Not found." });
            }
            var passwordIsValid = bcrypt.compareSync(
                req.body.password + req.body.npm,
                user.password
            );

            if (!passwordIsValid) {
                return res.status(401).send({
                    accessToken: null,
                    message: "Invalid Password!"
                });
            }

            var token = jwt.sign({ npm: user.npm }, config.secret, {
                expiresIn: 86400 // 24 hours
            });
            Config.findOne({ where: { id: 1 } }).then(c => {
                let navs = []
                try {
                    navs = c.navigations.split(',')
                } catch (e) {
                    navs = []
                }
                res.status(200).send({
                    npm: user.npm,
                    accessToken: token,
                    navs: navs
                });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
};

exports.refreshToken = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    let token = req.headers.authorization;
    if (!token) {
        return res.status(403).send({
            message: "No token provided!"
        });
    }

    token = token.split(' ')[1];

    jwt.verify(token, config.secret, { ignoreExpiration: true }, (err, decoded) => {
        User.findOne({
            where: {
                npm: decoded.npm
            }
        })
            .then(user => {
                if (!user) {
                    return res.status(404).send({ message: "User Not found." });
                }

                var token = jwt.sign({ npm: user.npm }, config.secret, {
                    expiresIn: 86400 // 24 hours
                });

                Config.findOne({ where: { id: 1 } }).then(c => {
                    let navs = []
                    try {
                        navs = c.navigations.split(',')
                    } catch (e) {
                        navs = []
                    }
                    res.status(200).send({
                        npm: user.npm,
                        accessToken: token,
                        navs: navs
                    });
                }).catch(err => {
                    console.log(err);
                    res.status(500).send({ message: err.message });
                });

            })
            .catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
    });

};

exports.changePassword = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({
        where: {
            npm: req.npm
        }
    }).then(user => {
        if (!user) {
            return res.status(404).send({ message: "User Not found." });
        }
        var passwordIsValid = bcrypt.compareSync(
            req.body.password + req.npm,
            user.password
        );

        if (!passwordIsValid) {
            return res.status(401).send({
                accessToken: null,
                message: "Invalid Password!"
            });
        } else {
            User.update({
                password: bcrypt.hashSync(req.body.newPassword + req.npm, 8)
            }, {
                where: { npm: req.npm }
            }).then(user => {
                console.log(`[auth password change][${new Date()}] ${req.npm} change password`);
                res.send({ message: "Password changed successfully!" });
            }).catch(err => {
                res.status(500).send({ message: err.message });
            });
        }
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.resetPassword = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({
        where: {
            npm: req.body.npm
        }
    }).then(user => {
        if (!user) {
            return res.status(404).send({ message: "User Not found." });
        }
        User.update({
            password: bcrypt.hashSync("5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8" + req.body.npm, 8)
        }, {
            where: { npm: req.body.npm }
        }).then(user => {
            console.log(`[auth password reset][${new Date()}] ${req.npm} reser password of ${req.body.npm}`);
            res.send({ message: "Password reset successfully!" });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.resetPasswordServer = (npm) => {
    User.findOne({
        where: {
            npm: npm
        }
    }).then(user => {
        User.update({
            password: bcrypt.hashSync("5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8" + npm, 8)
        }, {
            where: { npm: npm }
        }).then(user => {
            console.log(`[auth password reset][${new Date()}] server reser password of ${npm}`);
        }).catch(err => {
            console.log(err);
        });
    }).catch(err => {
        console.log(err);
    });
};