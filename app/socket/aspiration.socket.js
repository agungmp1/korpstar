// const { Server } = require("socket.io
const socketIO = require('socket.io');

const db = require("../models");
// const jwt = require("jsonwebtoken");
// const config = require("../config/auth.config.js");

const User = db.user;
const Role = db.role;
const Vote = db.vote;
const Aspiration = db.aspiration;

let aspirationio;

exports.attach = (server) => {
    aspirationio = socketIO(server,  {
        path: '/socket/aspiration',
        cors: {
            orign: 'http://localhost',
            methods: ["GET", "POST"],
            allowedHeaders: ["Access-Control-Allow-Origin", 'authorization']
        }
    });

    aspirationio.on('connection', (socket) => {
        let ip = '[hidden ip]'
        if (socket.conn.remoteAddress) {
            if (socket.conn.remoteAddress.split(':').length==4) {
                ip = socket.conn.remoteAddress.split(':')[3];
            }
        }
        console.log(`[${new Date()}] connection request on aspiration socket from ${ip}`);
        Aspiration.findAll({
            where: {isAccepted: true},
            attributes: ['id', 'title', 'argument', 'suggestion', 'proof', 'isAnonymous', 'isActive', 'createdAt'],
            include: [{
                model: User,
                attributes: ['npm', 'fullname']
            },{
                model: Vote,
                attributes: ['value']
            }]
        }).then( aspirations => {
            let aspirationData = [];
            aspirations.forEach((aspiration) => {
                let myvote = 0;
                let votecount = 0;
                aspiration.votes.forEach((vote) => {
                    votecount += vote.value;
                });
                aspirationData.push({
                    id: aspiration.id,
                    title: aspiration.title,
                    argument: aspiration.argument,
                    suggestion: aspiration.suggestion,
                    proof: aspiration.proof,
                    isAnonymous: aspiration.isAnonymous,
                    isActive: aspiration.isActive,
                    votecount: votecount,
                    createdAt: aspiration.createdAt,
                    user: aspiration.isAnonymous? {npm: '', fullname: 'Anonymous'}:aspiration.user,
                    myvote: myvote
                });
            });
            console.log(`[${new Date()}] user connected to aspiration socket from ${ip}`);
            socket.emit('connected', {status: 200, data: aspirationData, message: 'you are listening for aspiration events'})
            socket.join("aspirations")
        }).catch(err => {
            console.log(err);
            socket.emit('connected', {status: 500, message: err});
        });
    })
}

exports.emitdata = (eventname, data) =>{
    aspirationio.sockets.in("aspirations").emit(eventname, data)
}
