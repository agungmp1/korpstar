module.exports = (sequelize, Sequelize) => {
    const CTFChallengeCompletion = sequelize.define("ctf_challenge_completions", {
        writeup: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });

    return CTFChallengeCompletion;
};
