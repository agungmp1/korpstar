module.exports = (sequelize, Sequelize) => {
    const Role = sequelize.define("fouls", {
        name: {
            type: Sequelize.STRING,
            unique: true
        },
        point: {
            type: Sequelize.INTEGER
        }
    }, { timestamps: false });

    return Role;
};
