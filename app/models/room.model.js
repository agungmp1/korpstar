module.exports = (sequelize, Sequelize) => {
    const Room = sequelize.define("rooms", {
        number: {
            type: Sequelize.STRING
        },
        isChecked: {
            type: Sequelize.BOOLEAN
        }
    }, { timestamps: false });

    return Room;
};
