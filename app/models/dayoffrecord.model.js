module.exports = (sequelize, Sequelize) => {
    const DayoffRecord = sequelize.define("dayoff_records", {
        type: {
            type: Sequelize.ENUM(['Pesiar', 'Bermalam']),
            allowNull: false
        },
        pesiartype: {
            type: Sequelize.ENUM(['Pesiar pagi', 'Pesiar siang']),
            allowNull: true
        },
        date: {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        destinationaddress: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        transportation: {
            type: Sequelize.ENUM(['Embarkasi', 'Mandiri']),
            allowNull: true
        },
        selfcontact: {
            type: Sequelize.STRING,
            allowNull: false
        },
        destinationcontact: {
            type: Sequelize.STRING,
            allowNull: true
        },
        // status: {
        //     type: Sequelize.STRING,
        //     allowNull: false
        // },
        // departuretime: {
        //     type: Sequelize.DATE,
        //     allowNull: true
        // },
        // arrivaltime: {
        //     type: Sequelize.DATE,
        //     allowNull: true
        // }
    });

    return DayoffRecord;
};
