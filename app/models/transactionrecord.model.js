module.exports = (sequelize, Sequelize) => {
    const TransactionRecord = sequelize.define("transaction_records", {
        npm: {
            type: Sequelize.STRING,
            allowNull: false
        },
        value: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        type: {
            type: Sequelize.STRING,
            allowNull: false
        },
        status: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });

    return TransactionRecord;
};
