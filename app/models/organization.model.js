module.exports = (sequelize, Sequelize) => {
    const Organization = sequelize.define("organizations", {
        name: {
            type: Sequelize.STRING,
            unique: true
        }
    }, { timestamps: false });

    return Organization;
};
