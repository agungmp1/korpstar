module.exports = (sequelize, Sequelize) => {
    const Peleton = sequelize.define("peletons", {
        name: {
            type: Sequelize.STRING
        },
        shortname: {
            type: Sequelize.STRING
        }
    });

    return Peleton;
};
