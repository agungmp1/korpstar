module.exports = (sequelize, Sequelize) => {
    const Config = sequelize.define("configs", {
        navigations: {
            type: Sequelize.STRING
        },
        fastingopen: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        fastingdate: {
            type: Sequelize.STRING
        },
        malecommerceopen: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        femalecommerceopen: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        roomcheckingopen: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        roomcheckingdate: {
            type: Sequelize.STRING
        },
        roomcheckingpassword: {
            type: Sequelize.STRING
        },
        foulpassword: {
            type: Sequelize.STRING
        },
        dayoffopen: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        dayoffdate: {
            type: Sequelize.DATE
        },
        pamongoffset: {
            type: Sequelize.INTEGER
        },
        version: {
            type: Sequelize.STRING
        }
    });

    return Config;
};
