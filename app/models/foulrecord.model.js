module.exports = (sequelize, Sequelize) => {
    const FoulRecord = sequelize.define("foul_records", {
        date: {
            type: Sequelize.DATEONLY
        }
    }, { timestamps: false });

    return FoulRecord;
};
