module.exports = (sequelize, Sequelize) => {
    const Vote = sequelize.define("votes", {
        value: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    });

    return Vote;
};
