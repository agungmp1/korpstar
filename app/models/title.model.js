module.exports = (sequelize, Sequelize) => {
    const Title = sequelize.define("titles", {
        name: {
            type: Sequelize.STRING,
            unique: true
        },
        privLevel: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 1
        }
    }, { timestamps: false });
    return Title;
};
