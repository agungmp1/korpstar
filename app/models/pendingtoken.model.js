module.exports = (sequelize, Sequelize) => {
    const PendingToken = sequelize.define("pending_tokens", {
        transactiontoken:{
            type: Sequelize.STRING,
            allowNull: false
        }
    });
    return PendingToken;
};
