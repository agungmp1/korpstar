module.exports = (sequelize, Sequelize) => {
    const CTFCategory = sequelize.define("ctf_categories", {
        name: {
            type: Sequelize.STRING,
            unique: true
        }
    }, { timestamps: false });

    return CTFCategory;
};
