const { Telegraf } = require('telegraf');

const bot = new Telegraf('5240056437:AAEDm-nR0x3NUZEeToPkBsNvJis7oBz3mwI');

let chatIds = new Set()

bot.command('start', ctx => {
    console.log(`[BOT LISTENER][${new Date()}]new listener request: ${ctx.chat.id}`)
    console.log(ctx.chat);
    // bot.telegram.sendMessage(ctx.chat.id, "Halo Semuanyaa!\nKenalin namaku Respin, aku adalah pembawa pesan dari aplikasi Karya SuperApp. Kalau ada pesan penting nanti aku kirim langsung kesini yaa. tapi.. sebaiknya ngga ada sih\n(｡◕‿‿◕｡).\n\nBye Bye semoga kita ga sering-sering ketemu yaw   ʕ·͡ᴥ·ʔ");
    // chatIds.add(ctx.chat.id)
})

const { sendNotificationToAll } = require('./fcm.js')

bot.on('text', (ctx) => {
    // Explicit usage
    if (ctx.message.from.id == 1016024960 || ctx.message.from.id == 797161782) {
        title = ctx.message.text.split(/\r\n|\r|\n/)[0]
        sendNotificationToAll({
            notification: {
                title: title,
                body: ctx.message.text
            }
        })
        console.log(`[Anouncement Sent][${new Date()}] ${ctx.message.from.id} send new anouncement with title ${title}`);
    } else {
        console.log(`[Anouncement Request][${new Date()}] ${ctx.message.from.id} request new anouncement with title ${title}`);
    }
})

exports.sendDeathMessage = (name) => {
    randomMessages = [
        'Jangan berhenti memaknai!',
        'Jangan takut menghadapi!',
        'Hadapi, hayati, nikmti prosesnya!',
        'Tidak ada kata terlambat untuk menjadi lebih baik',
        'Tantangan ada untuk membentuk kita menjadi lebih cerdas, lebih tangkas, lebih tajam',
        'Jadilah tajam namun tidak melukai!',
        'Jadilah cerdas tanpa menggurui!'
    ]
    let message = `🚨🚨🚨🚨🚨🚨\n${name} terdeteksi telah melewati batas maksimal pelanggaran! \nHarap tunggu tindak lanjut dari Polisi Taruna.👮‍♂👮‍♂👮\nSemoga ${name} bisa menjadi lebih baik kedepannya.\n\n${randomMessages[Math.floor(Math.random() * 7)]}`
    chatIds.forEach((chatId) => {
        bot.telegram.sendMessage(chatId, message);
    });
}

exports.sendMessage = (name) => {
    let message = `test`

    chatIds.forEach((chatId) => {
        // bot.telegram.sendVideo(chatId, {file_id: 'BAACAgUAAxkBAAESTFpiQEytgnvvERJQR5-ehUbaxvbyUwAC2wUAApWHAVYcE5bmBxa6fyME'})
        bot.telegram.sendMessage(chatId, message);
    });
}

exports.launch = () => {
    chatIds = new Set()
    // chatIds.add('-528760836')           //grup pt
    chatIds.add('-1001415327250')       //taruna poltek
    try {
        bot.stop('prepare for relaunch')
    } catch (e) {
        console.log("no bot detected");
    } finally {
        try {
            bot.launch();
        } catch (error) {
            console.log(error);
        }
        console.log("bot launched");
    }
    // try {
    //     bot.launch();
    // } catch (error) {
    //     console.log(error);
    // }
    // }
}
